package com.cineplex.cineplexreservationservice.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "show_times", schema = "cineplex")
public class ShowTimesEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, columnDefinition = "serial")
	private Long id;

	//@NotNull
	@Column(name = "show_time_option_id")
	private Long showTime;

	@Column(name = "status")
	private String status;

	@ManyToOne
	@JoinColumn(name = "movie_id")
	private MoviesEntity movieId;

	//@NotEmpty
	//@NotNull
	@Size(min = 1, max = 10, message = "Seat List Size Error.")
	@Basic(fetch = FetchType.LAZY)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "showTimeId"/*, orphanRemoval = true*/)
	private List<SeatsEntity> seatsEntityList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public MoviesEntity getMovieId() {
		return movieId;
	}

	public void setMovieId(MoviesEntity movieId) {
		this.movieId = movieId;
	}

	//@NotEmpty
	public List<SeatsEntity> getSeatsEntityList() {
		return seatsEntityList;
	}

	public void setSeatsEntityList(List<SeatsEntity> seatsEntityList) {
		this.seatsEntityList = seatsEntityList;

		for (SeatsEntity seatsEntity : seatsEntityList) {
			seatsEntity.setShowTimeId(this);
		}
	}

	public Long getShowTime() {
		return showTime;
	}

	public void setShowTime(Long showTime) {
		this.showTime = showTime;
	}

}
