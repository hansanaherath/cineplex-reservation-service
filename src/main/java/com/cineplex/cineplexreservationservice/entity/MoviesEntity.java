package com.cineplex.cineplexreservationservice.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "movie", schema = "cineplex")
public class MoviesEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, columnDefinition = "serial")
	private Long id;

	//@NotBlank(message = "Name Can Not Be null or Empty.")
	//@NotNull(message = "Name Can Not Be null or Empty.")
	@Column(name = "name")
	private String name;

	//@NotBlank(message = "Description Can Not Be null or Empty.")
	//@NotNull(message = "Description Can Not Be null or Empty.")
	@Column(name = "description")
	private String description;

	@Column(name = "status")
	private String status;

	//@NotEmpty(message = "Show Times Can Not Empty.")
	//@Size(min=1 ,max = 3 ,message = "Show Time List Size Error.")
	@Basic(fetch = FetchType.LAZY)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "movieId"/*, orphanRemoval = true*/)
	private List<ShowTimesEntity> showTimesEntityList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<ShowTimesEntity> getShowTimesEntityList() {
		return showTimesEntityList;
	}

	public void setShowTimesEntityList(List<ShowTimesEntity> showTimesEntityList) {
		this.showTimesEntityList = showTimesEntityList;

        for(ShowTimesEntity b : showTimesEntityList) {
            b.setMovieId(this);
        }
	}
}
