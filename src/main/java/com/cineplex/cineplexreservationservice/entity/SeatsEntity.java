package com.cineplex.cineplexreservationservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "seats", schema = "cineplex")
public class SeatsEntity {

	//private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, columnDefinition = "serial")
	private Long id;

	//@NotNull
	@Column(name = "seat_number")
	private Integer seatNumber;

	//@NotNull
	@Column(name = "reserved_status")
	private String isReserved;

	@Column(name = "status")
	private String status;

	@ManyToOne
	@JoinColumn(name = "show_times_id")
	private ShowTimesEntity showTimeId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(Integer seatNumber) {
		this.seatNumber = seatNumber;
	}

	public String getIsReserved() {
		return isReserved;
	}

	public void setIsReserved(String isReserved) {
		this.isReserved = isReserved;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ShowTimesEntity getShowTimeId() {
		return showTimeId;
	}

	public void setShowTimeId(ShowTimesEntity showTimeId) {
		this.showTimeId = showTimeId;
	}

}
