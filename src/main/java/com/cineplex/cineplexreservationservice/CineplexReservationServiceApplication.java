package com.cineplex.cineplexreservationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CineplexReservationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CineplexReservationServiceApplication.class, args);
	}

}
