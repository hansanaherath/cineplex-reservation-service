package com.cineplex.cineplexreservationservice.services.userservices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cineplex.cineplexreservationservice.entity.MoviesEntity;
import com.cineplex.cineplexreservationservice.entity.SeatsEntity;
import com.cineplex.cineplexreservationservice.entity.ShowTimesEntity;
import com.cineplex.cineplexreservationservice.entity.ShowTimesOptionsEntity;
import com.cineplex.cineplexreservationservice.models.ShowTimesModel;
import com.cineplex.cineplexreservationservice.models.UpdateModel;
import com.cineplex.cineplexreservationservice.repository.MoviesRepository;
import com.cineplex.cineplexreservationservice.repository.ShowTimesOptionRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private MoviesRepository movieRepository;

	@Autowired
	private ShowTimesOptionRepository showTimesOptionRepository;

	@Autowired
	private ShowTimesOptionRepository showTimeOptionRepository;

	@Transactional
	@Override
	public void addNewMovies(ArrayList<MoviesEntity> movieModel) throws Exception {
		movieRepository.saveAll(movieModel);
		movieRepository.flush();
	}

	@Transactional
	@Override
	public void delete(List<Long> movieids) throws Exception {
		List<MoviesEntity> moviesList = movieRepository.findAllById(movieids);
		movieRepository.deleteAll(moviesList);
		movieRepository.flush();
	}

	@Transactional
	@Override
	public void updateMovies(List<Long> movieids, ArrayList<MoviesEntity> moviesList) throws Exception {
		delete(movieids);
		/*ArrayList<MoviesEntity> movieEntityList = new ArrayList<>();
		for (UpdateModel updateModel : moviesList) {
			MoviesEntity movie = new MoviesEntity();
			movie.setId(updateModel.getId());
			movie.setName(updateModel.getName());
			movie.setDescription(updateModel.getDescription());
			movie.setStatus(updateModel.getStatus());

			ArrayList<ShowTimesEntity> showTimesEntityList = new ArrayList<>();
			for (ShowTimesModel showTimesModel : updateModel.getShowTimesEntityList()) {
				ShowTimesEntity showTimesEntity = new ShowTimesEntity();
				showTimesEntity.setId(showTimesModel.getId());
				showTimesEntity.setShowTime(showTimesModel.getShowTime());
				showTimesEntity.setStatus(showTimesModel.getStatus());
				showTimesEntity.setMovieId(movie);

				showTimesEntity.setSeatsEntityList(showTimesModel.getSeatsEntityList());

				showTimesEntityList.add(showTimesEntity);
			}
			movie.setShowTimesEntityList(showTimesEntityList);
			movieEntityList.add(movie);
		}*/
		movieRepository.saveAll(moviesList/*movieEntityList*/);
		movieRepository.flush();
	}

	@Transactional
	@Override
	public HashMap<String, Object> getAllMovies() throws Exception {
		List<MoviesEntity> list = movieRepository.findAll();
		HashMap<String, Object> finalMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> movieLists = new ArrayList<HashMap<String, Object>>();

		HashMap<Long, String> showMapOptionMap = new HashMap<Long, String>();
		List<ShowTimesOptionsEntity> showTimeOptionsList = showTimeOptionRepository.findByStatus("Y");

		for (ShowTimesOptionsEntity showTimesOptionsEntity : showTimeOptionsList) {
			showMapOptionMap.put(showTimesOptionsEntity.getId(), showTimesOptionsEntity.getValue());
		}

		for (MoviesEntity moviesEntity : list) {
			HashMap<String, Object> responseMap = new HashMap<String, Object>();

			responseMap.put("id", moviesEntity.getId());
			responseMap.put("name", moviesEntity.getName());
			responseMap.put("description", moviesEntity.getDescription());
			responseMap.put("status", moviesEntity.getStatus());

			List<HashMap<String, Object>> listMap = new ArrayList<HashMap<String, Object>>();

			for (ShowTimesEntity showTimeEntity : moviesEntity.getShowTimesEntityList()) {
				if (showTimeEntity.getStatus().equals("Y")) {
					HashMap<String, Object> showTimeMap = new HashMap<String, Object>();

					showTimeMap.put("id", showTimeEntity.getId());
					showTimeMap.put("showTime", showTimeEntity.getShowTime());
					showTimeMap.put("movieId", showTimeEntity.getMovieId().getId());
					showTimeMap.put("status", showTimeEntity.getStatus());
					showTimeMap.put("showTimeOptionValue", showMapOptionMap.get(showTimeEntity.getShowTime()));

					List<HashMap<String, Object>> seatList = new ArrayList<HashMap<String, Object>>();

					for (SeatsEntity seat : showTimeEntity.getSeatsEntityList()) {
						if (seat.getStatus().equals("Y")) {
							HashMap<String, Object> seatMap = new HashMap<String, Object>();

							seatMap.put("id", seat.getId());
							seatMap.put("isReserved", seat.getIsReserved());
							seatMap.put("seatNumber", seat.getSeatNumber());
							seatMap.put("showTimeId", seat.getShowTimeId().getId());
							seatMap.put("status", seat.getStatus());

							seatList.add(seatMap);
						}
					}

					showTimeMap.put("seatList", seatList);

					listMap.add(showTimeMap);
				}

			}
			responseMap.put("showTimeEntityList", listMap);

			movieLists.add(responseMap);
		}
		finalMap.put("data", movieLists);

		return finalMap;
	}

	@Override
	public List<ShowTimesOptionsEntity> getAllShowTimeOptions() throws Exception {
		List<ShowTimesOptionsEntity> showTimesOptionsEntityList = showTimesOptionRepository.findByStatus("Y");
		return showTimesOptionsEntityList;
	}

}
