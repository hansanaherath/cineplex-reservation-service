package com.cineplex.cineplexreservationservice.services.userservices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.cineplex.cineplexreservationservice.entity.MoviesEntity;
import com.cineplex.cineplexreservationservice.entity.ShowTimesOptionsEntity;
import com.cineplex.cineplexreservationservice.models.UpdateModel;

public interface UserService {

	void addNewMovies(ArrayList<MoviesEntity> movieModel) throws Exception;

	void delete(List<Long> movieids) throws Exception;

	void updateMovies(List<Long> movieids, ArrayList<MoviesEntity> moviesList) throws Exception;

	HashMap<String, Object> getAllMovies() throws Exception;

	List<ShowTimesOptionsEntity> getAllShowTimeOptions() throws Exception;

}
