package com.cineplex.cineplexreservationservice.services.customerservices;

import java.util.HashMap;

import com.cineplex.cineplexreservationservice.entity.MoviesEntity;

public interface CustomerService {

	HashMap<String, Object> getAllAvailbleMovieList() throws Exception;

	void reservedSeats(Long movieid, MoviesEntity moviesEntity) throws Exception;

	void updateSeat(Long seatid) throws Exception;

}
