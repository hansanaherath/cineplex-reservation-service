package com.cineplex.cineplexreservationservice.services.customerservices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cineplex.cineplexreservationservice.entity.MoviesEntity;
import com.cineplex.cineplexreservationservice.entity.SeatsEntity;
import com.cineplex.cineplexreservationservice.entity.ShowTimesEntity;
import com.cineplex.cineplexreservationservice.entity.ShowTimesOptionsEntity;
import com.cineplex.cineplexreservationservice.repository.MoviesRepository;
import com.cineplex.cineplexreservationservice.repository.SeatRepository;
import com.cineplex.cineplexreservationservice.repository.ShowTimesOptionRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private MoviesRepository movieRepository;

	@Autowired
	private SeatRepository seatRepository;
	
	@Autowired
	private ShowTimesOptionRepository showTimeOptionRepository;

	@Transactional
	@Override
	public HashMap<String, Object> getAllAvailbleMovieList() throws Exception {
		List<MoviesEntity> list = movieRepository.findByStatus("Y");
		HashMap<String, Object> finalMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> movieLists = new ArrayList<HashMap<String, Object>>();
		
		List<ShowTimesOptionsEntity> showTimeOptionsList = showTimeOptionRepository.findByStatus("Y");
		HashMap<Long, String> showMapOptionMap = new HashMap<Long, String>();
		
		for(ShowTimesOptionsEntity showTimesOptionsEntity : showTimeOptionsList) {
			showMapOptionMap.put(showTimesOptionsEntity.getId(), showTimesOptionsEntity.getValue());
		}

		for (MoviesEntity moviesEntity : list) {
			HashMap<String, Object> responseMap = new HashMap<String, Object>();

			responseMap.put("id", moviesEntity.getId());
			responseMap.put("name", moviesEntity.getName());
			responseMap.put("description", moviesEntity.getDescription());
			responseMap.put("status", moviesEntity.getStatus());

			List<HashMap<String, Object>> listMap = new ArrayList<HashMap<String, Object>>();

			for (ShowTimesEntity showTimeEntity : moviesEntity.getShowTimesEntityList()) {
				if (showTimeEntity.getStatus().equals("Y")) {
					HashMap<String, Object> showTimeMap = new HashMap<String, Object>();
					
					
					
					showTimeMap.put("id", showTimeEntity.getId());
					showTimeMap.put("showTime", showMapOptionMap.get(showTimeEntity.getShowTime()));
					showTimeMap.put("movieId", showTimeEntity.getMovieId().getId());
					showTimeMap.put("status", showTimeEntity.getStatus());
					showTimeMap.put("showTimeOptionId", showTimeEntity.getShowTime());

					List<HashMap<String, Object>> seatList = new ArrayList<HashMap<String, Object>>();

					for (SeatsEntity seat : showTimeEntity.getSeatsEntityList()) {
						if (seat.getStatus().equals("Y")) {
							HashMap<String, Object> seatMap = new HashMap<String, Object>();
							
							seatMap.put("id", seat.getId());
							seatMap.put("isReserved", seat.getIsReserved());
							seatMap.put("seatNumber", seat.getSeatNumber());
							seatMap.put("showTimeId", seat.getShowTimeId().getId());
							seatMap.put("status", seat.getStatus());

							seatList.add(seatMap);
						}
					}

					showTimeMap.put("seatList", seatList);
					listMap.add(showTimeMap);
				}

			}
			responseMap.put("showTimeEntityList", listMap);
			movieLists.add(responseMap);
		}
		finalMap.put("data", movieLists);

		return finalMap;
	}

	@Transactional
	@Override
	public void reservedSeats(Long movieid, MoviesEntity moviesEntity) throws Exception {
		movieRepository.saveAndFlush(moviesEntity);
	}

	@Transactional
	@Override
	public void updateSeat(Long seatid) throws Exception {
		SeatsEntity seatObj = new SeatsEntity();
		seatObj.setId(seatid);
		
		Optional<SeatsEntity> seatEntityList = seatRepository.findById(seatid);

		if (seatEntityList.get().getIsReserved().equals("Y")) {
			seatEntityList.get().setIsReserved("N");
		} else {
			seatEntityList.get().setIsReserved("Y");
		}

		seatRepository.saveAndFlush(seatEntityList.get());
	}

}
