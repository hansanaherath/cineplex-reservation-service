package com.cineplex.cineplexreservationservice.models;

import java.util.List;


public class MoviesModel {

	private Long id;

	//@NotBlank
	private String name;

	//@NotBlank
	private String description;

	//@NotBlank
	//@Size(min = 1, max = 1, message = "Status wrong")
	private String status;

	//@NotEmpty
	//@Size(min = 1, max = 3, message = "Max Length exceed")
	private List<ShowTimesModel> ShowTimesModelList;

	/*private List<SeatsEntity> seatList;*/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<ShowTimesModel> getShowTimesModelList() {
		return ShowTimesModelList;
	}

	public void setShowTimesModelList(List<ShowTimesModel> showTimesModelList) {
		ShowTimesModelList = showTimesModelList;
	}

	/*public List<SeatsEntity> getSeatList() {
		return seatList;
	}

	public void setSeatList(List<SeatsEntity> seatList) {
		seatList = seatList;
	}*/
}
