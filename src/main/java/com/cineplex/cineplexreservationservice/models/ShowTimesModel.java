package com.cineplex.cineplexreservationservice.models;

import java.util.List;

import com.cineplex.cineplexreservationservice.entity.SeatsEntity;

public class ShowTimesModel {

	private Long id;

	private Long showTime;

	private String status;

	private List<SeatsEntity> seatsEntityList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShowTime() {
		return showTime;
	}

	public void setShowTime(Long showTime) {
		this.showTime = showTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SeatsEntity> getSeatsEntityList() {
		return seatsEntityList;
	}

	public void setSeatsEntityList(List<SeatsEntity> seatsEntityList) {
		this.seatsEntityList = seatsEntityList;
	}

}
