package com.cineplex.cineplexreservationservice.models;

import java.util.ArrayList;

public class UpdateModel {

	private Long id;
	private String name;
	private String description;
	private String status;
	private ArrayList<ShowTimesModel> showTimesEntityList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<ShowTimesModel> getShowTimesEntityList() {
		return showTimesEntityList;
	}

	public void setShowTimesEntityList(ArrayList<ShowTimesModel> showTimesEntityList) {
		this.showTimesEntityList = showTimesEntityList;
	}

}
