package com.cineplex.cineplexreservationservice.controllers.customercontroller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cineplex.cineplexreservationservice.entity.MoviesEntity;
import com.cineplex.cineplexreservationservice.services.customerservices.CustomerService;

@RestController
public class CustomerController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/api/v1/view/movies", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<HashMap<String, Object>> getMovieList() {
		UUID uuid = UUID.randomUUID();

		logger.info("<< Start </api/v1/view/movies getMovieList > Controller >> " + uuid);

		HashMap<String, Object> movieList = new HashMap<String, Object>();
		try {
			movieList = customerService.getAllAvailbleMovieList();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<< Error getMovieList >> " + uuid + "<>" + e);
			return new ResponseEntity<>(movieList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(movieList, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/v1/view/movies{movieid}", method = RequestMethod.PUT)
	public ResponseEntity<String> updateMovies(@PathVariable("movieid") Long movieid, @RequestBody MoviesEntity moviesEntity) {
		UUID uuid = UUID.randomUUID();
		logger.info("<< Start </api/v1/movies/{movieids} updateMovies> Controller >> " + uuid);
		try {
			customerService.reservedSeats(movieid, moviesEntity);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<< Error updateMovies>> " + uuid + "<>" + e);
			return new ResponseEntity<>("Data Update Failed.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("Data Updated Succesfully.", HttpStatus.OK);
	}

	@RequestMapping(value = "/api/v1/view/updateSeat/{seatid}", method = RequestMethod.PUT)
	public ResponseEntity<String> updateSeat(@PathVariable("seatid") Long seatid) {
		UUID uuid = UUID.randomUUID();
		logger.info("<< Start </api/v1/view/updateSeat/{seatid} updateSeat> Controller >> " + uuid);
		try {
			customerService.updateSeat(seatid);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<< Error updateSeat>> " + uuid + "<>" + e);
			return new ResponseEntity<>("Data Update Failed.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("Data Updated Succesfully.", HttpStatus.OK);
	}

}
