package com.cineplex.cineplexreservationservice.controllers.usercontrollers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cineplex.cineplexreservationservice.entity.MoviesEntity;
import com.cineplex.cineplexreservationservice.entity.ShowTimesOptionsEntity;
import com.cineplex.cineplexreservationservice.models.UpdateModel;
import com.cineplex.cineplexreservationservice.services.userservices.UserService;

@RestController
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/api/v1/movies", method = RequestMethod.POST)
	public ResponseEntity<String> addNewMovies( @RequestBody ArrayList<MoviesEntity> movieModel, BindingResult br) {
		UUID uuid = UUID.randomUUID();
		logger.info("<< Start </api/v1/movies addNewMovies > Controller >> " + uuid);

		if (br.hasErrors()) {
			logger.error("<< Validation Error </api/v1/movies addNewMovies > Controller >> " + uuid);
			return new ResponseEntity<>(br.getFieldError().getDefaultMessage(), HttpStatus.BAD_REQUEST);
		}

		try {
			userService.addNewMovies(movieModel);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<< Error addNewMovies >> " +uuid + "<>" + e);
			return new ResponseEntity<>("Data Save Failed.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("Data Saved Succesfully.", HttpStatus.OK);
	}

	@RequestMapping(value = "/api/v1/movies/{movieids}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteMovies(@PathVariable("movieids") List<Long> movieids) {
		UUID uuid = UUID.randomUUID();
		logger.info("<< Start </api/v1/movies/{movieids} deleteMovies > Controller >> " + uuid);
		try {
			userService.delete(movieids);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<< Error deleteMovies >> " +uuid + "<>" + e);
			return new ResponseEntity<>("Data Delete Failed.", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>("Data Deleted Succesfully.", HttpStatus.OK);
	}

	@RequestMapping(value = "/api/v1/movies", method = RequestMethod.GET , produces="application/json")
	public ResponseEntity<HashMap<String, Object>> viewMovies() {
		UUID uuid = UUID.randomUUID();
		logger.info("<< Start </api/v1/movies viewMovies> Controller >> " + uuid);
		HashMap<String, Object> movieList = new HashMap<String, Object>();

		try {
			movieList = userService.getAllMovies();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<< Error viewMovies>> " +uuid + "<>" + e);
			return new ResponseEntity<>(movieList,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(movieList, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/v1/movies/{movieids}", method = RequestMethod.PUT)
	public ResponseEntity<String> updateMovies(@PathVariable("movieids") List<Long> movieids, @RequestBody ArrayList<MoviesEntity/*UpdateModel*/> moviesList) {
		UUID uuid = UUID.randomUUID();
		logger.info("<< Start </api/v1/movies/{movieids} updateMovies> Controller >> " + uuid);
		System.out.println("dddddddddddddddddddddddddddd");
		try {
			userService.updateMovies(movieids, moviesList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<< Error updateMovies>> " +uuid + "<>" + e);
			return new ResponseEntity<>("Data Update Failed.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("Data Updated Succesfully.", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/v1/showTimeOptions", method = RequestMethod.GET , produces="application/json")
	public ResponseEntity<List<ShowTimesOptionsEntity>> getShowTimeOptions() {
		UUID uuid = UUID.randomUUID();
		logger.info("<< Start </api/v1/showTimeOptions getShowTimeOptions> Controller >> " + uuid);

		List<ShowTimesOptionsEntity> showTimesOptionsEntityList = new ArrayList<ShowTimesOptionsEntity>();
		try {
			showTimesOptionsEntityList  = userService.getAllShowTimeOptions();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<< Error getShowTimeOptions>> " +uuid + "<>" + e);
			return new ResponseEntity<>(showTimesOptionsEntityList,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(showTimesOptionsEntityList, HttpStatus.OK);
	}
}
