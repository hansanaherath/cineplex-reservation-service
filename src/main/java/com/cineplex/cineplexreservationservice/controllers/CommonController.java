package com.cineplex.cineplexreservationservice.controllers;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonController {

	@RequestMapping(value="/servicecheck", method=RequestMethod.GET)
	public String serviceCheck() {
		return "This is service-check";
	}
}
