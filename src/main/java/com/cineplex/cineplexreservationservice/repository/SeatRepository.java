package com.cineplex.cineplexreservationservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cineplex.cineplexreservationservice.entity.SeatsEntity;


public interface SeatRepository extends JpaRepository<SeatsEntity, Long>{

	Optional<SeatsEntity> findById(Long id);

	//SeatsEntity findOne(SeatsEntity seatObj);


}
