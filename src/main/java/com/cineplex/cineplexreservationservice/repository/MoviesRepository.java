package com.cineplex.cineplexreservationservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cineplex.cineplexreservationservice.entity.MoviesEntity;


public interface MoviesRepository extends JpaRepository<MoviesEntity, Long>{

	List<MoviesEntity> findByStatus(String status);

}
