package com.cineplex.cineplexreservationservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cineplex.cineplexreservationservice.entity.ShowTimesOptionsEntity;


public interface ShowTimesOptionRepository extends JpaRepository<ShowTimesOptionsEntity, Long>{

	List<ShowTimesOptionsEntity> findByStatus(String string);

}
